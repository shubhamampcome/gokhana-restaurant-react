var token = null;
var user = null;

export default {

  getToken() {
    return token;
  },

  setToken(newtoken, newuser) {
    token = newtoken;
    user = newuser;
  },

  clearToken() {
    token = null
    user = null
  },

  getUser(){
    return user;
  }
 
};