import React, { Component } from 'react';
import {css} from 'glamor';
import {Header} from '../common';

export default class Layout extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Header {...this.props}/>
        { this.props.children }
      </div>
    );
  }
}
