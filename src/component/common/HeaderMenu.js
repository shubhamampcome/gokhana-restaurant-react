import React, { Component } from 'react';
import {css} from 'glamor';
import {Menu} from '../partials/menuitems';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';
  import {
    BrowserRouter as Router,
    Route,
    Switch, Redirect, Link
  } from 'react-router-dom';

export default class HeaderMenu extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div {...css({backgroundColor: '#a4b7c1'})}>
        <Link to="/home/menuitems/items" className = 'btn btn-secondary' activeClassName="active" >Menu Items</Link>
        <Link to="/home/menuitems/addonitems" className ='btn btn-secondary' activeClassName="active">Addon Items</Link>
        <Link to="/home/menuitems/customize" className ='btn btn-secondary' activeClassName="active">Customize Items</Link>
        <Link to="/home/menuitems/categories" className ='btn btn-secondary' activeClassName="active">Categories</Link>
        <Link to="/home/menuitems/subcategories" className ='btn btn-secondary' activeClassName="active">Sub Categories</Link>
      </div>
    );
  }
}
