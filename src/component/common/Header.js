import React, { Component } from 'react';
import {css} from 'glamor';
import {auth} from '../../actions';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem, Button } from 'reactstrap';

  import {
    BrowserRouter as Router,
    Route,
    Switch, Redirect, Link
  } from 'react-router-dom';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div {...css({backgroundColor: 'Gold'})}>
      <a href="/"><img width="13%" className="mt-1 mr2" src="http://www.gokhana.com/images/logo1.png"/></a>
          <div className="ml5" {...css({display: 'inline-block'})}>
            <Link to="/home" className ='btn btn-primary' {...css({backgroundColor: '	#8B4513'})} activeClassName="active" >Home</Link>
            <Link to="/home/menuitems" className ='btn btn-primary' onClick={(e) => this.props.changecheck()} activeClassName="active">Menu Items</Link>
            <Link to="/home/orders" className ='btn btn-primary' activeClassName="active">Orders</Link>
            <Link to="/home/tablerequests" className ='btn btn-primary' activeClassName="active">Table Requests</Link>
            <Link to="/home/customers" className ='btn btn-primary' activeClassName="active">Customers</Link>
            <Button color="danger" className ='fr-ns' onClick={async (e) => { e.preventDefault(); await auth.logout(); this.props.history.push(`/`)}}>Log Out</Button>{' '}
          </div>
      </div>
    );
  }
}
