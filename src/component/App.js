import React, {Component} from 'react';
import {css} from 'glamor';
import {
  BrowserRouter as Router,
  Route,
  Switch, Redirect, Link
} from 'react-router-dom';

import {Container} from 'reactstrap';

import {Footer, Header, Layout} from './common';

import {Auth,Splash, MenuItems, Dashboard} from './screens';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';  

// Styles
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
  // Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../../scss/style.scss'
import axios from 'axios';
import {session} from '../ext';  

axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('token'));
axios.defaults.baseURL = "http://localhost:3000/api/gokhana";
  
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      check : "initial value"
    };
    this.changecheck = this.changecheck.bind(this);
  }

  changecheck(){
   this.setState({
     check : "new value"})
  }

render() {
  return (
    <div className="app">
    {console.log(this.state.check)}
      <Layout changecheck={this.changecheck} {...this.props}>
      <div className="app-body">
            <Route exact path={`${this.props.match.path}`} component={(props) => {
              return (<Dashboard {...props} />);
            }} />

            <Route path={`${this.props.match.path}/menuitems`} component={(props) => {
              return (<MenuItems changecheck={() => this.changecheck()} {...props} />);
            }} />

            <Route path={`${this.props.match.path}/orders`} component={(props) => {
              return (<Orders {...props} />);
            }} />

            <Route path={`${this.props.match.path}/tablerequests`} component={(props) => {
              return (<TableRequests {...props} />);
            }} />

            <Route path={`${this.props.match.path}/customers`} component={(props) => {
              return (<Customers {...props} />);
            }} />
      </div>
      </Layout>
    </div>
  );
}

}
