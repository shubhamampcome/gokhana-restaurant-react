import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {css} from 'glamor';
import {menuitems} from '../../../actions';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
// import classNames from 'classnames';

export default class CustomizeItems extends Component {

  constructor(props) {
    super(props);
    this.state = {
      menudata: []
    };
    this.checkdata = this.checkdata.bind(this)
    this.getmenudata = this.getmenudata.bind(this)
  }

  async componentWillMount() {
    let result = await this.getmenudata()
    this.setState({
      menudata : result
    })
  }

  async getmenudata(){
    var data = [];
    data = await menuitems.getcustomizeitems();
    return data;
  }

  checkdata(){
    if(this.props.data){
      return this.props.data.menuitems
    }
    else{
      return [];
    }
  }

  render() {
    return (
      <div>
      <BootstrapTable data={this.props.data.customizeitems} version='4' condensed pagination search tableContainerClass='my-custom-class'>
        <TableHeaderColumn isKey={true} dataField='parentId'>Parent Id</TableHeaderColumn>
        <TableHeaderColumn dataField='name'>Name</TableHeaderColumn>
        <TableHeaderColumn dataField='price'>Price</TableHeaderColumn>
        <TableHeaderColumn dataField='active'>Is Active</TableHeaderColumn>
      </BootstrapTable>
      </div>
      );
    }
  }