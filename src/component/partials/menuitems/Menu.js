import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {css} from 'glamor';
import {menuitems} from '../../../actions';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
// import classNames from 'classnames';

export default class Menu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      menudata: []
    };
    this.checkdata = this.checkdata.bind(this)
    this.getmenudata = this.getmenudata.bind(this)
  }

  // async componentWillMount() {
  //   let result = await this.getmenudata()
  //   this.setState({
  //     menudata : result
  //   })
  // }

  async getmenudata(){
    var data = [];
    data = await menuitems.getmenuitems();
    return data;
  }

  checkdata(){
    console.log(this.props.data)
    // if(this.props.data){
    //   return this.props.data.menuitems
    // }
    // else{
    //   return [];
    // }
  }

  render() {
    return (
      <div>
        {/* {this.checkdata()} */}
      <BootstrapTable data={this.props.data.menuitems} resize={{extra: 200}} version='4' condensed pagination search tableContainerClass='my-custom-class'>
        <TableHeaderColumn dataField='category'>Category</TableHeaderColumn>
        <TableHeaderColumn dataField='subcategory'>subcategory</TableHeaderColumn>
        <TableHeaderColumn dataField='name'>name</TableHeaderColumn>
        <TableHeaderColumn dataField='description'>description</TableHeaderColumn>
        <TableHeaderColumn dataField='price'>price</TableHeaderColumn>
        <TableHeaderColumn dataField='isVeg'>isVeg</TableHeaderColumn>
        <TableHeaderColumn dataField='isSpicy'>isSpicy</TableHeaderColumn>
        <TableHeaderColumn dataField='active'>active</TableHeaderColumn>
        <TableHeaderColumn isKey={true} dataField='taxgroup'>taxgroup</TableHeaderColumn>
      </BootstrapTable>
      </div>
      );
    }
  }