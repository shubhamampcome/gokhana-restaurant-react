import React, { Component } from 'react';
import {css} from 'glamor';
import isemail from 'is-email';
import {auth} from '../../actions';
import {Alert, CardImg, Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon} from "reactstrap";
// import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import '../../css/auth.css';

export default class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      serverErr: null
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  async componentWillMount() {
    let result = await auth.check()
    if (result) {
      this.props.history.push('/home');
    // } else {
    //   this.props.history.push('/login');
    }
  }

  validateForm() {
    return isemail(this.state.email) && this.state.password.length > 0;
  }

  handleChangeEmail = event => {
    this.setState({
      email: event.target.value
    });
  }

  handleChangePassword = event => {
    this.setState({
      password: event.target.value
    });
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    let result  = await auth.login(this.state.email, this.state.password)
    if(result === true){
      this.props.history.push('/home')
    }
    else {
      this.setState({serverErr : 12})
    }
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBlock className="card-body">
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                      <Input type="text" placeholder="Email" value={this.state.email}
                        onChange={this.handleChangeEmail}/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                      <Input type="password" placeholder="Password" value={this.state.password}
                        onChange={this.handleChangePassword}/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4" onClick={this.handleSubmit}>Login</Button>
                      </Col>
                    </Row>
                    {this.state.serverErr ? (<Alert className="mt-3"color="danger">Email or password is wrong </Alert>) : null}
                  </CardBlock>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 80 + '%' }}>
                {/* <img width="90%" className="mt-4 fl6" src="http://www.gokhana.com/images/logo1.png"/> */}
                <CardImg top width="100%" className="mt-4" src="http://www.gokhana.com/images/logo1.png" />
                  <CardBlock className="card-body text-center">
                    <div>
                      <h3>GoKhana Restaurant</h3>
                    </div>
                  </CardBlock>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

//   render() {
//     return (
//       <div className="Login" {...css({
//           backgroundColor: '#F0E68C'
//         })}>
//         <form onSubmit={this.handleSubmit}>
//           <FormGroup controlId="email" bsSize="large">
//             <ControlLabel>Email</ControlLabel>
//             <FormControl
//               autoFocus
//               type="email"
//               value={this.state.email}
//               onChange={this.handleChange}
//             />
//           </FormGroup>
//           <FormGroup controlId="password" bsSize="large">
//             <ControlLabel>Password</ControlLabel>
//             <FormControl
//               value={this.state.password}
//               onChange={this.handleChange}
//               type="password"
//             />
//           </FormGroup>
//           <Button {...css({
//             backgroundColor: '#FFA500'
//             })}
//             bsSize="large"
//             disabled={!this.validateForm()}
//             type="submit"
//           >
//             Login
//           </Button>
//         </form>
//         {this.state.serverErr ? (<div>Email id or password is wrong.</div>) : null}
//       </div>
//     );
//   }
// }