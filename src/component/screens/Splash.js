import React from 'react';
import {css} from 'glamor';
import proptypes from 'prop-types'

import {auth} from '../../actions';

export default class Splash extends React.Component {
  static propTypes = {
    history: proptypes.object
  }

 async componentWillMount() {
    let result = await auth.check()
    if (result) {
      this.props.history.push('/dashboard');
    } else {
      this.props.history.push('/auth');
    }
  }

  render() {
    return (
        <div {...css({
          width: '200px',
          height: '200px',
          position: 'absolute',
          left: '50%',
          top: '50%',
          marginLeft: '-100px',
          marginTop: '-100px'
        })}>
          <h1 {...css({textAlign: 'center'})}>Gokhana Restaurant</h1>
        </div>
    );
  }
}
