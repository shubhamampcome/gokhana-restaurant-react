import React, { Component } from 'react';
import {AddonItems, Categories, CustomizeItems, Menu, SubCategories} from '../partials/menuitems';
import {
  BrowserRouter as Router,
  Route,
  Switch, Redirect, Link
} from 'react-router-dom';
import {menuitems} from '../../actions';

import {HeaderMenu} from '../common';

export default class MenuItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menudata: [],
      hasFetched: false
    };
    this.check = this.check.bind(this);
    this.getmenudata = this.getmenudata.bind(this);
  }

  async componentDidMount() {
      let result = await this.getmenudata()
      this.setState({
        menudata : result,
        hasFetched: true
      })
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if ( this.state.hasFetched ) {
  //     return false;
  //   }
  //   return true;
  // }

  async getmenudata(){
    var data = {};
    data.menuitems = await menuitems.getmenuitems();
    data.addonitems = await menuitems.getaddonitems();
    data.customizeitems = await menuitems.getcustomizeitems();
    data.categories = await menuitems.getcategories();
    return data;
  }
  
  check(){
    if(this.props.location.pathname === '/home/menuitems')
    return (<Menu data = {this.state.menudata}{...this.props}/>)
  }

  render() {
    return (
      <div >
        <HeaderMenu{...this.props}/>
        {this.check()}
        
        <Route path={`${this.props.match.path}/items`} component={(props) => {
          return (<Menu data = {this.state.menudata}{...props} />);
        }} />

        <Route exact path={`${this.props.match.path}/addonitems`} component={(props) => {
          return (<AddonItems data = {this.state.menudata} {...props} />);
        }} />

        <Route exact path={`${this.props.match.path}/customize`} component={(props) => {
          return (<CustomizeItems data = {this.state.menudata} {...props} />);
        }} />

        <Route exact path={`${this.props.match.path}/categories`} component={(props) => {
          return (<Categories data = {this.state.menudata} {...props} />);
        }} />

        <Route exact path={`${this.props.match.path}/subcategories`} component={(props) => {
          return (<SubCategories data = {this.state.menudata} {...props} />);
        }} />
      </div>
    );
  }
}