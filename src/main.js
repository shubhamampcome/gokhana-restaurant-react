import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,HashRouter, Route, Switch} from 'react-router-dom';
import {createBrowserHistory} from 'history';
import {Auth,Splash, MenuItems, Dashboard} from './component/screens';
// import './index.css';
import './index.html';
import {App} from './component';
import {Layout} from './component/common';
// import registerServiceWorker from './registerServiceWorker';

// ReactDOM.render(<App/>, document.getElementById('root'));
// registerServiceWorker();
ReactDOM.render((
  <Router>
    <Switch>
      <Route exact path="/" name="Login Page" component={Auth}/>
      <Route path="/home" name="Home" component={App}/>
    </Switch>
  </Router>
), document.getElementById('root'));