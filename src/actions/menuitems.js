import axios from 'axios';
import {session} from '../ext';

export default {
  session,
  async getmenuitems() {
    try{
      let restaurantid = JSON.parse(localStorage.getItem('user')).entityrecordId
      let t = await axios.get(`/restaurant/${restaurantid}/menuitem`)
      return t.data
    }
    catch(err){
      console.log(err)
      return false
    }
  },

  async getaddonitems() {
    try{
      let restaurantid = JSON.parse(localStorage.getItem('user')).entityrecordId
      let t = await axios.get(`/restaurant/${restaurantid}/addonitem`)
      return t.data
    }
    catch(err){
      console.log(err)
      return false
    }
  },

  async getcustomizeitems() {
    try{
      let restaurantid = JSON.parse(localStorage.getItem('user')).entityrecordId
      let t = await axios.get(`/restaurant/${restaurantid}/customizationitem`)
      return t.data
    }
    catch(err){
      console.log(err)
      return false
    }
  },

  async getcategories() {
    try{
      let restaurantid = JSON.parse(localStorage.getItem('user')).entityrecordId
      let t = await axios.get(`/restaurant/${restaurantid}/menucategories`)
      return t.data
    }
    catch(err){
      console.log(err)
      return false
    }
  },

  async getsubcategories() {
    try{
      let restaurantid = JSON.parse(localStorage.getItem('user')).entityrecordId
      let t = await axios.get(`/restaurant/${restaurantid}`)
      return t.data
    }
    catch(err){
      console.log(err)
      return false
    }
  }
}

