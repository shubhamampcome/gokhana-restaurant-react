import axios from 'axios';
import {session} from '../ext';

export default {
  session,
  async check() {
    try{
      let restaurantid = JSON.parse(localStorage.getItem('user')).entityrecordId
      let t = await axios.get(`/restaurant/${restaurantid}`)
      return true
    }
    catch(err){
      console.log(err)
      return false
    }
  },

  async login(email, password) {
      let data = {
        email: email,
        password: password
      }
      try{
        delete axios.defaults.headers.common["Authorization"]; 
        let t = await axios.post('/login', data, {params:{include: "user"}})
        localStorage.setItem('token',JSON.stringify(t.data.id));
        localStorage.setItem('user', JSON.stringify(t.data.user));
        console.log(t.data.user)
        axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('token'));
        return true
      }
      catch(err){
        console.log(err)
        return false
      }
  },

  async logout() {
    try{
      // axios.defaults.headers.common['Authorization'] =  localStorage.getItem('token');
      let t = await axios.post('/logout')
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      return true
    }
    catch(err){
      console.log(err)
      return false
    }
  } 

}

